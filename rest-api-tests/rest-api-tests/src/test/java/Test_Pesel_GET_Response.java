import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_Pesel_GET_Response {
    @Test
    public void testGetRequest_response() {
        String expectedBody = "{\"pesel\":\"48100779844\",\"isValid\":true,\"dateOfBirth\":\"1948-10-07T00:00:00\",\"gender\":\"Female\",\"errors\":[]}";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=48100779844");
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody,expectedBody);
    }
}
