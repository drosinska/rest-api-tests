import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_Pesel_GET_ResponseElements_BirthDate {

    @Test
    public void testGetRequest_responseElements_BirthDate_1800_1899() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=20902065440");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1820-10-20T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_BirthDate_1900_1999() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=20102098684");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1920-10-20T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_BirthDate_2000_2099() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=20302365443");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2020-10-23T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_BirthDate_2100_2199() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=11502065446");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2111-10-20T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_BirthDate_2200_2299() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=50641598075");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2250-04-15T00:00:00");

    }
}
