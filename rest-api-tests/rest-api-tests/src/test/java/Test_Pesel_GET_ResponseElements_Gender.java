import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_Pesel_GET_ResponseElements_Gender {

    @Test
    public void testGetRequest_responseElements_Gender_0() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=21022800001");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Female");

    }

    @Test
    public void testGetRequest_responseElements_Gender_1() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=50101032213");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Male");

    }

    @Test
    public void testGetRequest_responseElements_Gender_2() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=50101036927");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Female");

    }

    @Test
    public void testGetRequest_responseElements_Gender_3() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=50101030631");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Male");

    }

    @Test
    public void testGetRequest_responseElements_Gender_4() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=11502065446");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Female");

    }

    @Test
    public void testGetRequest_responseElements_Gender_5() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=50101099058");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Male");

    }

    @Test
    public void testGetRequest_responseElements_Gender_6() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=50101075362");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Female");

    }

    @Test
    public void testGetRequest_responseElements_Gender_7() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=81013198078");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Male");

    }

    @Test
    public void testGetRequest_responseElements_Gender_8() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=20102098684");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Female");

    }

    @Test
    public void testGetRequest_responseElements_Gender_9() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=50101098590");
        String actualGender = actualResponse.path("gender");

        Assert.assertEquals(actualGender,"Male");

    }
}
