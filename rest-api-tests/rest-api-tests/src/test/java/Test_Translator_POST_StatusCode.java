import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.post;

public class Test_Translator_POST_StatusCode {
    @Test
    public void testPostRequest_StatusCode() {

        Response response = post("https://api.funtranslations.com/translate/pirate.json?text=Hello%20sir!");

        Assert.assertEquals(response.statusCode(),200);
    }
}

