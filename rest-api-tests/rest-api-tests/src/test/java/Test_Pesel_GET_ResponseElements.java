import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_Pesel_GET_ResponseElements {

    @Test
    public void testGetRequest_responseElements_errorCode_invalidLength() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=4810077984");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVL");
    }

    @Test
    public void testGetRequest_responseElements_errorCode_invalidCharacters() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=4810077984A");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"NBRQ");
    }

    @Test
    public void testGetRequest_responseElements_errorCode_invalidYear() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00013200002");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVY");
    }

    @Test
    public void testGetRequest_responseElements_errorCode_invalidMonth() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00013200002");
        String actualErrorCode = actualResponse.path("errors[1].errorCode");

        Assert.assertEquals(actualErrorCode,"INVM");
    }

    @Test
    public void testGetRequest_responseElements_errorCode_invalidDay() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00013200002");
        String actualErrorCode = actualResponse.path("errors[1].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");
    }

    @Test
    public void testGetRequest_responseElements_errorCode_invalidCheckSum() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=48100779845");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVC");
    }
}

