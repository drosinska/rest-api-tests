import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_CatFacts_GET_ResponseElements {

    @Test
    public void testGetRequest_responseElements_maxLength() {
        Response actualResponse = get("https://catfact.ninja/fact?max_length=30");

        Integer actualLength = actualResponse.path("length");

        Assert.assertTrue(actualLength < 30);
    }
}




