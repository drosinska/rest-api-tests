import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_Pesel_GET_StatusCode {

    @Test
    public void testGetRequest_ResponseStatusCodeOk() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=48100779844");

        Assert.assertEquals(response.statusCode(),200);

    }

    @Test
    public void testGetRequest_ResponseStatusCodeBadRequest() {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=");

        Assert.assertEquals(response.statusCode(),400);

    }
}
