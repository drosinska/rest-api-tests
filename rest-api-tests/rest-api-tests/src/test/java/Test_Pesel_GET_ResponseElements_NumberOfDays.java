import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_Pesel_GET_ResponseElements_NumberOfDays {

    @Test
    public void testGetRequest_responseElements_NumberOfDays_JanOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=81013198078");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1981-01-31T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_JanNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=81013298075");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_FebOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=21022800001");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1921-02-28T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_FebNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=21022900008");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_FebLeapOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=20022900001");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1920-02-29T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_FebLeapNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=20023000007");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_MarOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=81833198074");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1881-03-31T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_MarNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=81013298075");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_AprOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=11243098077");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2011-04-30T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_AprNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=11243198074");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_MayOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=12253100008");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2012-05-31T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_MayNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=11453298078");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_JunOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=12663000004");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2212-06-30T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_JunNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=12663100001");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_JulOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=12873100008");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1812-07-31T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_JulNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=12873200005");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_AugOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=11083101009");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1911-08-31T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_AugNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=11083201006");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_SepOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99293000006");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2099-09-30T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_SepNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99293100003");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_OctOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=67503100002");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2167-10-31T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_OctNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=67503200009");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_NovOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=33713000007");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"2233-11-30T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_NovNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=33713100004");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_DecOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=22923100005");
        String actualBirthDate = actualResponse.path("dateOfBirth");

        Assert.assertEquals(actualBirthDate,"1822-12-31T00:00:00");

    }

    @Test
    public void testGetRequest_responseElements_NumberOfDays_DecNotOk() {

        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=22923200002");
        String actualErrorCode = actualResponse.path("errors[0].errorCode");

        Assert.assertEquals(actualErrorCode,"INVD");

    }
}
