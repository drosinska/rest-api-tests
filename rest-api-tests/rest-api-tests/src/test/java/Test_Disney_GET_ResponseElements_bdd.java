import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.when;


public class Test_Disney_GET_ResponseElements_bdd {

    @Test
    public void testGetRequest_responseElements_name() {
        when()
            .get("https://api.disneyapi.dev/characters/455")
        .then()
            .extract()
            .path("name")
            .equals("Bambi");

    }
}

