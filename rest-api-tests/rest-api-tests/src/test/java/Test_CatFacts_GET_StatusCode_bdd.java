import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;


public class Test_CatFacts_GET_StatusCode_bdd {

    @Test
    public void testGetRequest_ResponseStatusCodeOk_bdd() {
        when()
            .get("https://catfact.ninja/fact?max_length=25")
        .then()
            .assertThat()
            .statusCode(200);
}


    @Test
    public void testGetRequest_ResponseStatusCodeNotFound() {
        when()
            .get("https://catfact.ninja/fac?max_length=25")
        .then()
            .assertThat()
            .statusCode(404);
    }
}
